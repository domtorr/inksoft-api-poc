﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InkSoftPOC.Models
{
    public class DebugDataContextLog : StringWriter
    {

        public override void Write(string value)
        {
            base.Write(value);
            LogSql(value);
        }

        public override System.Threading.Tasks.Task WriteAsync(string value)
        {
            LogSql(value);
            return base.WriteAsync(value);
        }

        public override void WriteLine(string value)
        {
            LogSql(value);
            base.WriteLine(value);
        }

        public override System.Threading.Tasks.Task WriteLineAsync(string value)
        {
            LogSql(value);
            return base.WriteLineAsync(value);
        }

        public void LogSql(string value)
        {
            base.Write(value);
            Debug.WriteLine($"DbContextLog: {value}");
        }
    }
}
