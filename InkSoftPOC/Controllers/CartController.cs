﻿using InkSoftPOC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;

namespace InkSoftPOC.Controllers
{
    public class CartController : ApiController
    {
        [HttpGet]
        public JsonResult<List<CartItemReport>> ListCartItems()
        {
            List<CartItemReport> report = new List<CartItemReport>();
            using (var dc = new CartDataContext())
            {
                foreach (var cart in dc.Carts)
                {
                    foreach (var cartItem in cart.CartItems)
                    {
                        report.Add(new CartItemReport()
                        {
                            CartId = cart.ID,
                            Email = cart.Email,
                            Price = cartItem.Product.Price,
                            ProductId = cartItem.ProductId,
                            ProductName = cartItem.Product.Name,
                            Quantity = cartItem.Quantity,
                            FrontImageUrl = cartItem.Product.ProductImages.FirstOrDefault().Url
                        });
                    }
                }
            }
            return Json(report);
        }

        [HttpGet]
        public JsonResult<List<CartReport>> ListCarts()
        {
            List<CartReport> report = new List<CartReport>();
            using (var dc = new CartDataContext())
            {
                foreach (var cart in dc.Carts)
                {
                    var reportItem = new CartReport()
                    {
                        CartId = cart.ID,
                        Email = cart.Email,
                        TotalPrice = 0,
                        TotalQuantity = 0
                    };

                    foreach (var cartItem in cart.CartItems)
                    {
                        reportItem.TotalPrice += cartItem.Product.Price;
                        reportItem.TotalQuantity += cartItem.Quantity;
                    }

                    report.Add(reportItem);
                }
            }
            return Json(report);
        }
    }
}
