USE master
GO
DROP DATABASE IF EXISTS InkSoftTest
CREATE DATABASE InkSoftTest
GO
USE InkSoftTest
GO
CREATE TABLE Cart (ID int NOT NULL PRIMARY KEY IDENTITY(1,1), Email varchar(255))
CREATE TABLE CartItem (ID int NOT NULL PRIMARY KEY IDENTITY(1,1), CartId int NOT NULL, ProductId int NOT NULL, Quantity int NOT NULL)
CREATE TABLE Product (ID int NOT NULL PRIMARY KEY IDENTITY(1,1), Price decimal(18,4) NOT NULL, Name varchar(255))
CREATE TABLE ProductImage (ID int NOT NULL PRIMARY KEY IDENTITY(1,1), Name varchar(255), ProductId int NOT NULL, Url varchar(255))

INSERT INTO Product (Price, Name) VALUES
(10.00, 'Ten Dollar Shirt'),
(20.00, 'Twenty Dollar Shirt'),
(30.00, 'Thirty Dollar Shirt'),
(40.00, 'Forty Dollar Shirt'),
(50.00, 'Fifty Dollar Shirt')


INSERT INTO ProductImage (ProductId, Name, Url) VALUES
(1, 'back','https://images.inksoft.com/images/products/2728/products/G200L/WHITE/back/500.png?decache='),
(1, 'front', 'https://images.inksoft.com/images/products/2728/products/G200L/WHITE/front/500.png?decache='),
(1, 'right','https://images.inksoft.com/images/products/2728/products/G200L/WHITE/sleeveleft/500.png?decache='),
(2, 'front', 'https://images.inksoft.com/images/products/2728/products/G200L/RED/front/500.png?decache='),
(2, 'back','https://images.inksoft.com/images/products/2728/products/G200L/RED/back/500.png?decache='),
(2, 'right','https://images.inksoft.com/images/products/2728/products/G200L/RED/sleeveleft/500.png?decache='),
(3, 'front', 'https://images.inksoft.com/images/products/2728/products/G200L/BLACK/front/500.png?decache='),
(3, 'back','https://images.inksoft.com/images/products/2728/products/G200L/BLACK/back/500.png?decache='),
(3, 'right','https://images.inksoft.com/images/products/2728/products/G200L/BLACK/sleeveleft/500.png?decache='),
(4, 'front', 'https://images.inksoft.com/images/products/2728/products/G200L/ROYAL/front/500.png?decache='),
(4, 'back','https://images.inksoft.com/images/products/2728/products/G200L/ROYAL/back/500.png?decache='),
(4, 'right','https://images.inksoft.com/images/products/2728/products/G200L/ROYAL/sleeveleft/500.png?decache='),
(5, 'back','https://images.inksoft.com/images/products/2728/products/G200L/ORANGE/back/500.png?decache='),
(5, 'right','https://images.inksoft.com/images/products/2728/products/G200L/ORANGE/sleeveleft/500.png?decache=')

INSERT INTO Cart (Email) VALUES ('jared@example.com'), ('barbara@example.com'), ('mike@example.com'), ('manish@example.com'), ('courtney@example.com')
INSERT INTO CartItem (CartID, ProductId, Quantity) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(1, 5, 1),
(2, 1, 2),
(2, 2, 2),
(2, 3, 2),
(2, 4, 2),
(2, 5, 2),
(3, 1, 3),
(3, 2, 3),
(3, 3, 3),
(3, 4, 3),
(3, 5, 3),
(4, 1, 4),
(4, 2, 4),
(4, 3, 4),
(4, 4, 4),
(4, 5, 4),
(5, 1, 5),
(5, 2, 5),
(5, 3, 5),
(5, 4, 5),
(5, 5, 5)